import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:intl/intl.dart';


class TaskList {
  String? heading;
  String? task;
  String? date;
  TaskList({this.heading, this.task, this.date});
}

class ToDoList extends StatefulWidget {
  const ToDoList({super.key});
  @override
  State createState() => _ToDoListState();
}

class _ToDoListState extends State<ToDoList> {
  TextEditingController titleController = TextEditingController();

  TextEditingController descriptionController = TextEditingController();

  TextEditingController dateController = TextEditingController();

  bool isEdit = true;
  int titleIdx = -1;

  var listofColors = [
    const Color.fromRGBO(250, 232, 232, 1),
    const Color.fromRGBO(232, 237, 250, 1),
    const Color.fromRGBO(250, 249, 232, 1),
    const Color.fromRGBO(250, 232, 250, 1),
  ];

  List ofAllTasks = [];

  Widget _bottomField(context) {
    if (isEdit == false) {
      titleController.text = ofAllTasks[titleIdx].heading;
      descriptionController.text = ofAllTasks[titleIdx].task;
      dateController.text = ofAllTasks[titleIdx].date;
    }
    return Padding(
      padding: MediaQuery.of(context).viewInsets,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(
            height: 10,
          ),
          Text(
            "Create Task",
            style: GoogleFonts.quicksand(
              fontWeight: FontWeight.bold,
              fontSize: 15,
            ),
          ),
          const SizedBox(
            height: 2,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                "  Title",
                style: GoogleFonts.quicksand(
                  color: const Color.fromRGBO(0, 139, 148, 1),
                  fontWeight: FontWeight.w400,
                  fontSize: 15,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              TextField(
                controller: titleController,
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: const BorderSide(
                      color: Color.fromRGBO(0, 130, 138, 1),
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: Colors.purpleAccent,
                    ),
                    borderRadius: BorderRadius.circular(12),
                  ),
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                "  Description",
                style: GoogleFonts.quicksand(
                  color: const Color.fromRGBO(0, 139, 148, 1),
                  fontWeight: FontWeight.w400,
                  fontSize: 15,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              TextField(
                controller: descriptionController,
                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: const BorderSide(
                      color: Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: Colors.purpleAccent,
                    ),
                    borderRadius: BorderRadius.circular(12),
                  ),
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              Text(
                "  Date",
                style: GoogleFonts.quicksand(
                  color: const Color.fromRGBO(0, 139, 148, 1),
                  fontWeight: FontWeight.w400,
                  fontSize: 15,
                ),
              ),
              const SizedBox(
                height: 5,
              ),
              TextField(
                controller: dateController,
                readOnly: true,
                decoration: InputDecoration(
                  suffixIcon: const Icon(Icons.date_range_rounded),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(12),
                    borderSide: const BorderSide(
                      color: Color.fromRGBO(0, 139, 148, 1),
                    ),
                  ),
                  border: OutlineInputBorder(
                    borderSide: const BorderSide(
                      color: Colors.purpleAccent,
                    ),
                    borderRadius: BorderRadius.circular(12),
                  ),
                ),
                onTap: () async {
                  DateTime? pickeddate = await showDatePicker(
                    context: context,
                    initialDate: DateTime.now(),
                    firstDate: DateTime(2024),
                    lastDate: DateTime(2025),
                  );
                  String formatedDate = DateFormat.yMMMd().format(pickeddate!);
                  setState(() {
                    dateController.text = formatedDate;
                  });
                },
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            height: 30,
            width: 250,
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(30)),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ),
                backgroundColor: const Color.fromRGBO(0, 139, 148, 1),
              ),
              onPressed: () {
                setState(() {
                  titleController.text.trim();
                  descriptionController.text.trim();
                  dateController.text.trim();
                  if (isEdit == false) {
                    isEdit = true;

                    if (titleController.text.isNotEmpty &&
                        descriptionController.text.isNotEmpty &&
                        dateController.text.isNotEmpty) {
                      ofAllTasks[titleIdx].heading = titleController.text;
                      ofAllTasks[titleIdx].task = descriptionController.text;
                      ofAllTasks[titleIdx].date = dateController.text;

                      Navigator.pop(context);
                    }
                  } else if (titleController.text.isNotEmpty &&
                      descriptionController.text.isNotEmpty &&
                      dateController.text.isNotEmpty) {
                    ofAllTasks.add(TaskList(
                        heading: titleController.text,
                        task: descriptionController.text,
                        date: dateController.text));
                    titleController.clear();
                    descriptionController.clear();
                    dateController.clear();
                    Navigator.pop(context);
                  }
                });
              },
              child: Text(
                "Submit",
                style: GoogleFonts.inter(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 20,
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 30,
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      
      appBar: AppBar(
        title: const Text(
          "To-do list",
          style: TextStyle(
            fontSize: 26,
            fontWeight: FontWeight.w700,
            color: Color.fromRGBO(255, 255, 255, 1),
          ),
        ),
        centerTitle: true,
        backgroundColor: const Color.fromRGBO(2, 167, 177, 1),
        shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(30),
                bottomRight: Radius.circular(30),
              ),
            ),
        
      ),
      body: Padding(
        padding: const EdgeInsets.all(10.0),
        child: ListView.builder(
          itemCount: ofAllTasks.length,
          itemBuilder: (context, index) {
            return Container(
              padding: const EdgeInsets.all(10.0),
              height: 180,
              width: 330,
              margin: const EdgeInsets.symmetric(vertical: 10.0),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 2,
                ),
                color: listofColors[index % listofColors.length],
                boxShadow: const [
                  BoxShadow(
                    blurRadius: 20,
                    spreadRadius: 1,
                    offset: Offset(0, 10),
                    color: Color.fromRGBO(0, 0, 0, 1),
                  )
                ],
              ),
              child: Column(
                children: [
                  Row(
                    children: [
                      Container(
                        height: 52,
                        width: 52,
                        margin: const EdgeInsets.all(10),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          border: Border.all(width: 1.0),
                          boxShadow: const [
                            BoxShadow(
                                blurRadius: 10,
                                spreadRadius: 0,
                                color: Color.fromRGBO(0, 0, 0, 0.07)),
                          ],
                        ),
                        child: const CircleAvatar(
                            backgroundImage: NetworkImage(
                                "https://tse1.mm.bing.net/th?id=OIP.HGwGqH0hwUuKQqWn40hjOgHaHa&pid=Api&rs=1&c=1&qlt=95&w=121&h=121")),
                      ),
                      Expanded(
                        child: Column(
                          children: [
                            Container(
                              height: 20,
                              width: 243,
                              child: Text("${ofAllTasks[index].heading}",
                                  style: GoogleFonts.quicksand(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w600)),
                            ),
                            const SizedBox(
                              height: 10,
                            ),
                            Container(
                              height: 72,
                              width: 243,
                              child: Text("${ofAllTasks[index].task}",
                                  style: GoogleFonts.quicksand(
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                    color: const Color.fromRGBO(84, 84, 84, 1),
                                  )),
                            ),
                            const SizedBox(
                              height: 22,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Container(
                        height: 30,
                        width: 98,
                        child: Text(
                          "${ofAllTasks[index].date}",
                          style: GoogleFonts.quicksand(
                              fontSize: 15, fontWeight: FontWeight.w500),
                        ),
                      ),
                      const Spacer(flex: 1),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            titleController.clear();
                            descriptionController.clear();
                            dateController.clear();
                            isEdit = false;
                            titleIdx = index;
                            showModalBottomSheet(
                              isScrollControlled: true,
                              context: context,
                              builder: (BuildContext context) {
                                return _bottomField(context);
                              },
                            );
                          });
                        },
                        child: const Icon(
                          Icons.edit_outlined,
                          color: Color.fromRGBO(0, 139, 148, 1),
                        ),
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            ofAllTasks.removeAt(index);
                          });
                        },
                        child: const Icon(
                          Icons.delete_outline,
                          color: Color.fromRGBO(0, 139, 148, 1),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          isEdit = true;
          titleController.clear();
          descriptionController.clear();
          dateController.clear();
          showModalBottomSheet(
              isScrollControlled: true,
              context: context,
              builder: (BuildContext context) {
                return _bottomField(context);
              });
        },
        child: Icon(Icons.add),
      ),
    );
  }
}
