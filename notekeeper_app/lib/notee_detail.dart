import 'package:flutter/material.dart';
import 'package:notekeeper_app/model/note.dart';
import 'package:notekeeper_app/utilis/database_helper.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'package:intl/intl.dart';


class NoteDetail extends StatefulWidget {
  String appBartitle;
  NoteDetail(this.appBartitle);
  @override
  State createState() => NoteDetailState(this.appBartitle);
}

class NoteDetailState extends State<NoteDetail> {
  String appBartitle;
  static var _priorities = ['High', 'Low'];
  TextEditingController titleController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  NoteDetailState(this.appBartitle);
  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = TextStyle(
      // Define text style properties as needed
      color: Colors.black,
      fontSize: 16.0,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(appBartitle),
      /*  leading: IconButton(icon:Icon( 
          Icons.arrow_back),
          onPressed: (){
            moveToLastScreen();   
         },
        ), */
      ),
      body: Padding(
        padding: EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            ListTile(
              title: DropdownButton(
                items: _priorities.map((String dropDownStringItem) {
                  return DropdownMenuItem<String>(
                    value: dropDownStringItem,
                    child: Text(dropDownStringItem),
                  );
                }).toList(),
                style: textStyle,
                value: 'Low', // Changed to 'Low' as the default value
                onChanged: (valueSelectedByUser) {
                  setState(() {
                    debugPrint('USER SELECTED $valueSelectedByUser');
                  });
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15.0),
              child: TextField(
                controller: titleController,
                style: textStyle,
                onChanged: (value) {
                  debugPrint("Something changed in title text field");
                },
                decoration: InputDecoration(
                  labelText: 'Title',
                  labelStyle: textStyle,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 15.0),
              child: TextField(
                controller: descriptionController,
                style: textStyle,
                onChanged: (value) {
                  debugPrint("Something changed in description text field");
                },
                decoration: InputDecoration(
                  labelText: 'Description',
                  labelStyle: textStyle,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 15.0, bottom: 15.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: ElevatedButton(
                      child: Text('Save'),
                      onPressed: () {
                        setState(() {
                          debugPrint("Save Button Clicked");
                        });
                      },
                    ),
                  ),
                  SizedBox(width: 5.0),
                  Expanded(
                    child: ElevatedButton(
                      child: Text('DELETE'),
                      onPressed: () {
                        setState(() {
                          debugPrint("DELETE Button Clicked");
                        });
                      },
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
