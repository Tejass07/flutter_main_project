import 'package:flutter/material.dart';
import 'package:notekeeper_app/notee_detail.dart';
import 'package:notekeeper_app/model/note.dart';
import 'package:notekeeper_app/utilis/database_helper.dart';
import 'package:notekeeper_app/notee_detail.dart';
import 'package:sqflite/sqflite.dart';

class NoteList extends StatefulWidget{
  @override
  State createState()=> _NoteListState();
}

class _NoteListState extends State<NoteList>{ 
    DatabaseHelper databaseHelper = DatabaseHelper();
      late List<Note> noteList ;
      int count = 0;
      @override
      Widget build(BuildContext context){
        

        return Scaffold( 
          appBar: AppBar( 
             title: const Text('Notes'),
          ),
          body: getNoteListView(),

          floatingActionButton: FloatingActionButton( 
            onPressed: (){ 
              debugPrint('FAB clicked');
              NavigateTodetail('Add Note');
            },
            tooltip: 'Add Note',
            child: Icon(Icons.add),
          ),
        );
      }

ListView getNoteListView(){ 
     // TextStyle titleStyle = Theme.of(context).textTheme.subhead;
      
      return ListView.builder(
        itemCount: count ,
        itemBuilder:(BuildContext context , int position){ 

              return Card( 
                 color: Colors.white,
                 elevation: 2.0, 
                 child: ListTile( 
                  leading: CircleAvatar( 
                    backgroundColor: Colors.yellow,
                    child: Icon(Icons.keyboard_arrow_right),
                  ),
                  title:Text(' DummyText'),
                  subtitle: Text('Dummy Data '),
                  trailing:Icon(Icons.delete , color:Colors.grey),
                  onTap: (){
                    debugPrint("ListTitle Tapped");
                    NavigateTodetail('Edit note');
                  }
                 ),
              );

        },
      );
}

void NavigateTodetail( String title){
  Navigator.push(context , MaterialPageRoute(builder: (context){ 
    return NoteDetail(title);
  }));
}


}