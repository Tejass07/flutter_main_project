import 'package:sqflite/sqflite.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:notekeeper_app/model/note.dart';

class DatabaseHelper{
      static  DatabaseHelper _databaseHelper = DatabaseHelper._createInstance();
      static Database _database = DatabaseHelper._database ;
    

      String noteTable = 'note_table';
      String colId = 'id';
      String colTitle = 'title';
      String colDescription = 'description';
      String colPriority = 'priority';
      String colDate = 'date';

      DatabaseHelper._createInstance();

      factory DatabaseHelper(){
      _databaseHelper ??= DatabaseHelper._createInstance(); 
       return _databaseHelper;
      }
      Future<Database> get database async{
        _database ??= await initialzeDatabase();
        return _database;
      }

     Future<Database> initialzeDatabase() async{
        Directory directory = await getApplicationDocumentsDirectory();
        String path = directory.path + 'notes.db';

        var noteDatabase = await openDatabase(path,version: 1,onCreate: _createDb);
        return noteDatabase;
      }

      void _createDb(Database db , int newVersion) async{ 
        await db.execute('CREATE TABLE $noteTable($colId INTEGER PRIMARY KEY AUTOINCREMENT , $colTitle TEXT , $colDescription TEXT, $colPriority INTEGER , $colDate TEXT)');

      }

      Future<List<Map<String , dynamic>>> getNoteMapList() async{ 
        Database db = await this.database;

        var result = await db.rawQuery('SELECT = FROM $noteTable order by $colPriority ASC');
        return result;
      }

      Future<int> insertNote(Note note) async{
        Database db = await this.database;
        var result = await db.insert(noteTable, note.toMap());
        return result;
      }

      Future<int> updateNote(Note note) async{ 
        var db = await this.database;
        var result = await db.update(noteTable , note.toMap(),where: '$colId = 7' , whereArgs: [note.id]);

        return result;
      }
      Future<int> deleteNote(int id) async{
        var db = await this.database;
        int result = await db.rawDelete('DELETE FROM $noteTable WHERE $colId = $id');
        return result;
      }

      Future<int?> getCount() async{ 
        Database db = await this.database;
        List<Map<String,dynamic>> x = await db.rawQuery('SELECT COUNT (*) from $noteTable');
        int? result = Sqflite.firstIntValue(x);
        return result;
      }


}