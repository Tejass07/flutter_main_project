import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class loginPage extends StatefulWidget{ 
      
      const loginPage({super.key});

      @override
      State createState() => _loginPage(); 

}

class _loginPage extends State<loginPage>{ 

TextEditingController NameTextEditingController = TextEditingController();
TextEditingController userNameTextEditingController = TextEditingController();  
TextEditingController PasswordTextEditingController = TextEditingController();  
TextEditingController ConfirmPassTextEditingController = TextEditingController();  

      @override
      Widget build(BuildContext context){
        return Scaffold( 
          appBar: AppBar( 
            title: const Text("Login"),
            centerTitle: true,
          ),
          body: Center( 
            child: Padding( 
              padding: EdgeInsets.only( top: 10.0),
              child: Column( 
                children: [ 
                      SizedBox(
                        height: 58.82,
                        width: 69.76,
                        child: Image.network("https://tse1.mm.bing.net/th?id=OIP.oRbDmtv0ZvezLV1sEYNFowHaHa&pid=Api&rs=1&c=1&qlt=95&w=113&h=113"),
                      ),
                      SizedBox( height: 100,),

                     Container( 
                      height: 410,
                      width: 280,
                      child: Column( 
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [ 
                            SizedBox( 
                              height: 24,
                              width: 167,
                              child: const Text("Create your Account",style: TextStyle(fontSize: 16,fontWeight: FontWeight.w500),),
                            ),

                        Form(
                        child:Column( 
                         crossAxisAlignment: CrossAxisAlignment.center,
                         children: [ 
                          const SizedBox( 
                            height:20,
                          ),
                          TextFormField( 
                            controller: NameTextEditingController,
                            decoration: InputDecoration( 
                              hintText: 'Enter Name',
                              border: OutlineInputBorder( 
                                borderRadius: BorderRadius.circular(20),
                              )
                            ),
                          ),
                          const SizedBox( 
                            height: 10,
                          ),
                          TextFormField( 
                            controller: userNameTextEditingController,
                            decoration: InputDecoration( 
                              hintText: 'User Name',
                              border: OutlineInputBorder( 
                                borderRadius: BorderRadius.circular(20),
                              )
                            ),
                          ),
                          const SizedBox( 
                            height: 10,
                          ),
                          TextFormField( 
                            controller: PasswordTextEditingController,
                            decoration: InputDecoration( 
                              hintText: 'Enter Password',
                              border: OutlineInputBorder( 
                                borderRadius: BorderRadius.circular(20),
                              )
                            ),
                          ),
                          const SizedBox( 
                            height: 10,
                          ),
                          TextFormField( 
                            controller: ConfirmPassTextEditingController,
                            decoration: InputDecoration( 
                              hintText: 'Confirm Password',
                              border: OutlineInputBorder( 
                                borderRadius: BorderRadius.circular(20),
                              )
                            ),
                          ),
                          const SizedBox( 
                            height: 30,
                          ),

                          ElevatedButton(onPressed: (){}, child: const Text("Sign Up"),style: ButtonStyle(
                            
                          ),)



                         ], 
                        ), 
                        
                         ),   

                            
                        ],
                      ),

                     ), 
                      
                ],
              ),
            ),
          ),
        );
      }
}